/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.examenferreteriajee.dao;


import ec.edu.ups.examenferreteriajee.cliente2.Producto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Luis Mario
 */

@Stateless
public class ProductoDAO {
    
    
    @PersistenceContext
    private EntityManager em;
    
    
    public Producto buscarProducto (int id){
        
        return em.find(Producto.class, id);
        
    }
    
    public void actualizarStock (Producto producto)
    {
        em.merge(producto);
    }
    
}
