/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.examenferreteriajee.servicios;

import ec.edu.ups.examenferreteriajee.cliente2.Producto;
import ec.edu.ups.examenferreteriajee.dao.ProductoDAO;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Luis Mario
 */

@ApplicationPath("/rest")
@Path("/proveedor")
public class ServiciosWeb extends Application {
    @Inject
    private ProductoDAO prodao;
    
@GET
@Path("/solicitar")
@Produces(MediaType.APPLICATION_JSON)
public boolean solicitarProducto(@QueryParam("id") int id, @QueryParam("cantidad") int cantidad){
    Producto p = prodao.buscarProducto(id);
    
    
    if(p!=null){
        if(p.getStock()>=cantidad){
            
            p.setStock(p.getStock()-cantidad);
            prodao.actualizarStock(p);
            return true;
        }else{
            return false;
        }
    }
    return false;
}
    

@GET
@Path("/buscar")
@Produces(MediaType.APPLICATION_JSON)
public Producto buscarProducto(@QueryParam("id") int id){
    
    return prodao.buscarProducto(id);
}


}
