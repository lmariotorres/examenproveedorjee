/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.examenproveedor1jee.vistas;

import ec.edu.ups.examenferreteriajee.cliente2.Producto;
import ec.edu.ups.examenferreteriajee.cliente2.Proveedor;


import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Luis Mario
 */

@ManagedBean
@ViewScoped
public class InsertarProducto {
    
    @PostConstruct
    public void init(){
        producto = new Producto();
    }
    
    private Producto producto;
    
   

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public InsertarProducto() {
        
    }
    
    public String guardarProducto(){
        
        try {
            
         
        } catch (Exception ex) {
            Logger.getLogger(InsertarProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    private String proveedor;

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }
    
    
    
    
}
